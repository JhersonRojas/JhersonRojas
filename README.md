<img src="./assets/fondo.gif" width="100%" />

<h1 align="center" style="margin: 2rem 0 2rem 0;">
  Hi 👋, I'm Jherson Rojas
</h1>

<h3 align="center">
  A passionate FullStack developer from Colombia 
</h3>

### About

```
  Knowledgeable in web development in different areas, client, server and databases
```

### What I think

```
 "Life is too short to not want to know everything that living it entails"
```

### 🌐 Connect with me:

[<img src="./assets/job/linkedin.svg" height="50rem" width="50rem" />](https://co.linkedin.com/in/jhersonrojas/)

<h2 align="center" style="margin: 1rem 0 2rem 0;">
  Skills and knowledge in
</h2>

<h3 align="center">
  Languages with more experience
</h3>
<div align="center">

[<img src="./assets/langs/js.svg" width="60rem" height="60rem" />](https://developer.mozilla.org/es/docs/Web/JavaScript)
[<img src="./assets/langs/ts.svg" width="53rem" height="53rem" />](https://www.typescriptlang.org/)
[<img src="./assets/langs/py.svg" width="60rem" height="60rem" />](https://www.python.org/)
[<img src="./assets/langs/cs.svg" width="60rem" height="60rem" />](https://learn.microsoft.com/es-es/dotnet/csharp/)

</div>

<h3 align="center">
  Frontend development
</h3>
<div align="center">

[<img src="./assets/frontend/sass.svg" width="60rem" height="60rem" />](https://sass-lang.com/)
[<img src="./assets/frontend/css.svg" width="53rem" height="53rem" />](https://developer.mozilla.org/es/docs/Web/CSS)
[<img src="./assets/frontend/angular.svg" width="60rem" height="60rem" />](https://angular.io/)
[<img src="./assets/frontend/html.svg" width="60rem" height="60rem" />](https://html.com/)
[<img src="./assets/frontend/react.svg" width="60rem" height="60rem" />](https://react.dev/)

</div>

<h3 align="center">
  Backend development and databases
</h3>
<div align="center">

[<img src="./assets/backend/mariadb.svg" width="50rem" height="50rem" />](https://mariadb.org/)
[<img src="./assets/backend/next.svg" width="50rem" height="50rem" style="background-color: white; border-radius: 50%;" />](https://nextjs.org/)
[<img src="./assets/backend/docker.svg" width="55rem" height="55rem" />](https://www.docker.com/)
[<img src="./assets/backend/git.svg" width="55rem" height="55rem" />](https://git-scm.com/)
[<img src="./assets/backend/express.svg" width="35rem" height="35rem" style="background-color: white; padding: 0.5rem; border-radius: 50%;" />](https://expressjs.com/)
[<img src="./assets/backend/mysql.svg" width="60rem" height="60rem" />](https://www.mysql.com/)

</div>

<h3 align="center">
  Another tools
</h3>
<div align="center">

[<img src="./assets/others/vercel.svg" width="50rem" height="50rem" />](https://vercel.com/)
[<img src="./assets/others/jwt.svg" width="40rem" height="40rem" />](https://jwt.io/)
[<img src="./assets/others/linux.svg" width="50rem" height="50rem" />](https://www.google.com/)

</div>

<h3 align="center">
  📊 GitHub Stats
</h3>
<div align="center">

![](https://github-readme-stats.vercel.app/api/top-langs/?username=JhersonRojas&theme=dark&hide_border=false&include_all_commits=true&count_private=true&layout=compact) <br /> <br />
![](https://github-readme-streak-stats.herokuapp.com/?user=JhersonRojas&theme=dark&hide_border=false)

</div>
